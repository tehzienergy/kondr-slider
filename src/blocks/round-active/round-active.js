$('.round-active__slider').slick({
  slidesToShow: 2,
  responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});
